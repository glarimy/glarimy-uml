# OOAD and UML #

- Portal: https://www.glarimy.com/wip/ 
- Resources: https://bitbucket.org/glarimy/glarimy-uml/src/master/
- Java Patterns: https://bitbucket.org/glarimy/glarimy-java-patterns/src
- C++ Patterns: https://bitbucket.org/glarimy/glarimy-cpp-patterns/src/master/ 
- Design: https://bitbucket.org/glarimy/design/src/master/
- Take a Quiz: http://tech.glarimy.com/
- Ask Questions: krishna@glarimy.com
